import CardContainer from "../../Component/CardContainer/CardContainer"

const Home = ({ products, handleFavourite, handleAddToCart, handleRemoveFromCart }) => {
    return(
        <div>
            <CardContainer 
      products={products} 
      handleFavourite={handleFavourite} 
      handleAddToCart={handleAddToCart}
      handleRemoveFromCart={handleRemoveFromCart}
      />
        </div>
    )
}

export default Home