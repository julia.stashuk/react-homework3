import PropTypes from 'prop-types';
import "./ProductCard.scss"
import Button from '../Button/Button';
import SecondModal from '../SecondModal/SecondModal'
import React, { useState, useEffect } from 'react';
import StarImage from '../StarImage/StarImage'

const ProductCard = ({
    title = " ",
    id,
    price = " ",
    color = " ",
    image,
    handleFavourite, 
    isFavourite: initialIsFavourite = false,
    handleAddToCart,
    handleRemoveFromCart
}) => {
    const [isAddedToCart, setIsAddedToCart] = useState(false);
    const [secondModalOpen, setSecondModalOpen] = useState(false);
    const [isFavourite, setIsFavourite] = useState(initialIsFavourite);

    useEffect(() => {
        // При завантаженні компонента, оновіть значення isFavourite на основі даних з localStorage
        const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
        setIsFavourite(favorites.includes(id));
    }, [id]);

    useEffect(() => {
        const cart = JSON.parse(localStorage.getItem('cart')) || [];
        const found = cart.find(item => item.id === id);
        setIsAddedToCart(!!found);}, [id]);

    const handleSecondModalOpen = () => {
        setSecondModalOpen(true);
    };

    const handleCloseSecondModal = () => {
        setSecondModalOpen(false);
    };

    const handleStarClick =() => {
        handleFavourite(id)
        setIsFavourite(prevState => !prevState);
    }
    const handleButtonClick = () => {
        if (isAddedToCart) {
            handleRemoveFromCart(id);
        } else {
            handleSecondModalOpen();
        }
        setIsAddedToCart(prevState => !prevState); 
    };
    return (
        <div className="card">
            <div
                className={`svgWrapper ${isFavourite ? 'active' : ''}`}
                onClick={handleStarClick}
            >
                <StarImage isFavourite={isFavourite}/>
            </div>

            <img src={image} alt="product-image" className="image" />
            <h2 className="title">{title}</h2>
            
            <p className="price"> Price: {price} UAH</p>
            <span className="color"> {color}</span>

            <Button
            className={`main-button ${isAddedToCart ? 'added-to-cart' : ''}`}
            onClick={handleButtonClick}
            text={isAddedToCart ? "Added to Cart" : "Add to Cart"}
            isAddedToCart={isAddedToCart}
            handleAddToCart={handleAddToCart}
            ></Button>
        
            {secondModalOpen && (
                <SecondModal
                handleAddToCart={handleAddToCart}
                isAddedToCart={isAddedToCart}
                handleCloseSecondModal={handleCloseSecondModal}
                handleRemoveFromCart={handleRemoveFromCart}
                id={id}
                title = {title}
                price = {price}
            />)}
    </div>
    )
}


ProductCard.propTypes = {
    title: PropTypes.string,
    id: PropTypes.number.isRequired,
    price: PropTypes.number,
    color: PropTypes.string,
    image: PropTypes.string,
    handleFavourite: PropTypes.func,
    isFavourite: PropTypes.bool,
    handleAddToCart: PropTypes.func,
    handleRemoveFromCart: PropTypes.func
};

export default ProductCard;
