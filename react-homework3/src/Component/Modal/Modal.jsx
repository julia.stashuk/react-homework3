import React from 'react';
import './Modal.scss';
import PropTypes from 'prop-types';


const Modal = ({ header, text, image, handleCloseSecondModal, modalType,
    handleCloseFirstModal, backgroundColor, actions, closeButton, showImage }) => {
    const handleClick = (e) => {
        if (e.currentTarget.classList.contains("modal-wrapper")) {
            if (modalType === 'second') {
                handleCloseSecondModal();
            } else if (modalType === 'first') {
                handleCloseFirstModal();
            }
        }
    };

    return (
        <div className="modal-wrapper" onClick={handleClick}>
            <div className="modal" style={{ backgroundColor }}>
                <div className="modal-box">
                    {!!closeButton ? <p className='modal-close' onClick={handleClick}></p> : null}
                    <div className='modal-header'>
                        <h1 className="modal-title">{header}</h1>
                    </div>
                    {showImage && <img src={image} alt="Modal Image" className="modal-image" />}

                    <div className="modal-content">
                        <p>{text}</p>
                    </div>
                    <div className="modal-footer">
                        {actions}
                    </div>
                </div>
            </div>
        </div>
    );
};

Modal.propTypes = {
    
    handleCloseSecondModal: PropTypes.func, 
    modalType: PropTypes.oneOf(['first', 'second']).isRequired, 
    handleCloseFirstModal: PropTypes.func,
    closeButton: PropTypes.bool.isRequired, 
    showImage: PropTypes.bool.isRequired, 
};

export default Modal;