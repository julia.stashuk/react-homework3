
import PropTypes from 'prop-types';
import "./Menu.scss"
import StarImage from '../StarImage/StarImage';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

const Menu = ({ children, favoriteProducts, cart }) => {
    const [favoriteCount, setFavoriteCount] = useState(favoriteProducts.length);
    useEffect(() => {
        setFavoriteCount(favoriteProducts.length);
    }, [favoriteProducts]);
    
    const [cartItemCount, setCartItemCount] = useState(cart.length); // Додайте стан для кількості товарів у корзині
    useEffect(() => {
        setCartItemCount(cart.length); // Оновіть кількість товарів у корзині при зміні
    }, [cart]);

    return (
        <div className='menu'>
        <Link to="/"><div className='home'>HOME</div></Link>
        <div className="icons">
<span className="favorite-count">{favoriteCount}</span>
<Link to="/favorite"><StarImage className="img-star" /></Link>
<span className="cart-item-count">{cartItemCount}</span>
<Link to="/cart"><img src='../../public/shopping-cart.png' alt='Cart' className="img" /></Link>
{children}
</div>
</div>
    );
};

Menu.propTypes = {
    children: PropTypes.node,
    favoriteProducts: PropTypes.array.isRequired,
    cart: PropTypes.array.isRequired
};
export default Menu;
    