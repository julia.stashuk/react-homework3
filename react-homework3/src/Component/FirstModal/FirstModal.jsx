import React from 'react';
import Modal from '../Modal/Modal';
import Button from '../Button/Button';
import PropTypes from 'prop-types';

const FirstModal = ({ handleCloseFirstModal, title, handleRemoveFromCart = () => { }, image, item }) => {

  return (
    <Modal
    handleCloseFirstModal={handleCloseFirstModal}
    modalType="first"
      backgroundColor="rgba(213, 212, 194, 1)"
      closeButton={true}
      showImage={true}
      image={image}
      header={`Delete ${title}!`}
      text={`By clicking the "Yes, Delete" button, ${title} will be deleted.`}
      actions={[
        <Button
          key="cnsbtn"
          className="main-button"
          text="NO, CANCEL"
          onClick={handleCloseFirstModal}
        />,
        <Button
          key="okbtn"
          className="main-button"
          text="YES, DELETE"
          onClick={() => handleRemoveFromCart(item.id)}
        />
      ]}
    />
  );
}

FirstModal.propTypes = {
handleCloseFirstModal: PropTypes.func.isRequired,
handleRemoveFromCart: PropTypes.func.isRequired,
};
export default FirstModal;

