import PropTypes from 'prop-types';
import React, { useState } from 'react';
import Button from '../Button/Button';
import FirstModal from '../FirstModal/FirstModal';
import './CartItem.scss'

function CartItem({
    item = {},
    cart = [],
    products = [],
    handleRemoveFromCart = () => { },
    decrementCartItem = () => { },
    incrementCartItem = () => { },
}) {

    const [firstModalOpen, setFirstModalOpen] = useState(false);
    
    const handleFirstModalOpen = () => {
      setFirstModalOpen(true);
    };

    const handleCloseFirstModal = () => {
      setFirstModalOpen(false);
    };

    const cartItem = cart.find(cartItem => cartItem.id === item.id);
    const quantityInCart = cartItem ? cartItem.quantity : 0;

    const product = products.find(product => product.id === item.id);
    const totalPrice = product ? product.price * quantityInCart : 0;
    const roundedTotalPrice = totalPrice.toFixed(2);
   
    return (
        <div className="item">
          <div className='item-content'>
            <img className="product-image"  src={item.image} alt={item.title} />
            <h2 className="product-name">{item.title}</h2>
            <p className="product-price">Price: {item.price}</p>
          
          <div className='item-footer'>
            <button className="main-button count-button" onClick={() => { decrementCartItem(item.id) }}>-</button>
            <p className="quantity">{quantityInCart}</p>
            <button className="main-button count-button" onClick={() => { incrementCartItem(item.id) }}>+</button>
          </div>
          <p className="count">Total: {roundedTotalPrice}</p>
          <div className='item-close'>
            <button className="close-button" onClick={handleFirstModalOpen}>X</button>
          </div>
          </div>
            {firstModalOpen && (
                <FirstModal 
                handleCloseFirstModal={handleCloseFirstModal} 
                    onClick={handleCloseFirstModal}
                    item={item}
                    title={item.title}
                    image={item.image}
                    handleRemoveFromCart={handleRemoveFromCart}
                    
                />)}
            
        </div>
    );
}


CartItem.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string,
    image: PropTypes.string,
    price: PropTypes.number,
    quantity: PropTypes.number,
  }),
  cart: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      quantity: PropTypes.number.isRequired,
    })
  ),
  products: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string,
      image: PropTypes.string,
      price: PropTypes.number,
    })
  ),
  handleRemoveFromCart: PropTypes.func,
  decrementCartItem: PropTypes.func,
  incrementCartItem: PropTypes.func,
};
export default CartItem;
