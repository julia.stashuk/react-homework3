import React from 'react';
import Modal from '../Modal/Modal';
import Button from '../Button/Button';

const SecondModal = ({ handleCloseSecondModal, handleAddToCart, title, price, id}) => {
  const handleAddToCartClick = () => {
    handleAddToCart(id);
    handleCloseSecondModal();
};
  return (
    <Modal
    handleCloseSecondModal={handleCloseSecondModal}
    modalType="second"
    backgroundColor="rgba(213, 212, 194, 1)"
    closeButton={true}
    header={`Add Product "${title}"`}
    text={
      <span className="modal-text">
        {price}
      </span>
    }
    actions={ 
    <Button
      className="main-button"
      backgroundColor="rgba(151, 137, 12, 0.475)"
      text="ADD TO CART"
      onClick={handleAddToCartClick}
    />} 
  />   
  );
}

export default SecondModal;



  
  