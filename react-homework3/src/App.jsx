import React, { useEffect, useState } from 'react';
import './App.css';
import { useImmer } from 'use-immer';
import Menu from './Component/Menu/Menu';
import AppRouter from '../src/Router';

function App() {
  const [products, setProducts] = useImmer([]);
  const [favoriteProducts, setFavoriteProducts] = useState(() => {
    const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
    return favorites;
  });

  const [cart, setCart] = useImmer(() => {
    const savedCart = JSON.parse(localStorage.getItem('cart')) || [];
    return savedCart;
  });

  useEffect(() => {
    async function fetchProducts() {
      try {
        const response = await fetch('./products.json');
        if (!response.ok) {
          throw new Error('Failed to fetch products');
        }
        const data = await response.json();
        setProducts(data);
      } catch (error) {
        console.error('Error fetching products:', error.message);
      }
    }

    fetchProducts();
  }, []);

  useEffect(() => {
    const savedCart = JSON.parse(localStorage.getItem('cart')) || [];
    setCart(savedCart);
  }, [setCart]);

  useEffect(() => {
    const savedProducts = JSON.parse(localStorage.getItem('products')) || [];
    setProducts(savedProducts);
  }, []);

  const handleFavourite = (id) => {
    setProducts((draft) => {
      const product = draft.find((product) => id === product.id);
      product.isFavourite = !product.isFavourite;
      updateFavorites(id, product.isFavourite);
      localStorage.setItem('products', JSON.stringify(draft)); 
    });
  };

  const updateFavorites = (id, isFavourite) => {
    let favorites = [...favoriteProducts];
    if (isFavourite) {
      // Додавання товару до списку вибраних
      if (!favorites.includes(id)) {
        favorites.push(id);
        localStorage.setItem('favorites', JSON.stringify(favorites));
      }
    } else {
      // Видалення товару зі списку вибраних
      const updatedFavorites = favorites.filter((itemId) => itemId !== id);
      favorites = updatedFavorites;
      localStorage.setItem('favorites', JSON.stringify(favorites));
    }
    setFavoriteProducts(favorites);
  };

  const handleAddToCart = (id) => {
    setCart((draft) => {
      const cartItem = draft.find((item) => item.id === id);
      if (cartItem) {
        cartItem.quantity++;
      } else {
        draft.push({ id, quantity: 1 });
      }
      localStorage.setItem('cart', JSON.stringify(draft));
    });
  };

  const decrementCartItem = (itemId) => {
    setCart(draft => {
      const cartItem = draft.find(({ id }) => id === itemId);
      if (cartItem.quantity > 1) {
        cartItem.quantity--; 
        localStorage.setItem('cart', JSON.stringify(draft));
      }
    });
  };
  
  const incrementCartItem = (itemId) => {
    setCart(draft => {
      const cartItem = draft.find(({ id }) => id === itemId);
      cartItem.quantity++;
      localStorage.setItem('cart', JSON.stringify(draft));
    });
  };

  const handleRemoveFromCart = (id) => {
    setCart((draft) => {
      const index = draft.findIndex((item) => item.id === id);
      if (index !== -1) {
        const cartItem = draft[index];
        if (cartItem.quantity > 1) {
          cartItem.quantity--; 
        } else {
          draft.splice(index, 1);
        }
        localStorage.setItem('cart', JSON.stringify(draft));
      }
    });
  };

  return (
    <div>
      <Menu favoriteProducts={favoriteProducts} cart={cart} handleAddToCart={handleAddToCart}/>
      <AppRouter
        products={products}
        handleFavourite={handleFavourite}
        cart={cart}
        handleRemoveFromCart={handleRemoveFromCart}
        handleAddToCart={handleAddToCart}
        decrementCartItem={decrementCartItem}
        incrementCartItem={incrementCartItem}

      />
    </div>
  );
}

export default App;
